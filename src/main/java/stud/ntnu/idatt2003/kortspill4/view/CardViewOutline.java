package stud.ntnu.idatt2003.kortspill4.view;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class CardViewOutline {
  private Label cardValueLabel;
  private Label cardSuitLabel;
  private BorderPane card;

  public CardViewOutline(String cardValue, String cardSuit) {
    this.cardValueLabel = new Label(cardValue);
    this.cardSuitLabel = new Label(cardSuit);

    cardValueLabel.setFont(new Font("Arial", 30));
    cardSuitLabel.setFont(new Font("Arial", 30));

    card = new BorderPane();
    BorderStroke borderStroke = new BorderStroke(Color.BLACK,
            BorderStrokeStyle.SOLID,
            new CornerRadii(10),
            new BorderWidths(2));
    card.setBorder(new Border(borderStroke));
    card.setMaxHeight(100);
    card.setMinHeight(100);
    card.setMaxWidth(75);
    card.setMinWidth(75);
    cardValueLabel.setTextAlignment(TextAlignment.LEFT);
    card.setTop(cardValueLabel);
    card.setBottom(cardSuitLabel);

    BorderPane.setAlignment(cardSuitLabel, Pos.BOTTOM_RIGHT);
  }

  public BorderPane getCard() {
    return card;
  }
  public String getCardValue() {
    return cardValueLabel.getText();
  }
  public String getCardSuit() {
    return cardSuitLabel.getText();
  }
}

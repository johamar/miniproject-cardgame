package stud.ntnu.idatt2003.kortspill4;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import stud.ntnu.idatt2003.kortspill4.model.DeckOfCards;
import stud.ntnu.idatt2003.kortspill4.model.Hand;
import stud.ntnu.idatt2003.kortspill4.model.PlayingCard;
import stud.ntnu.idatt2003.kortspill4.view.CardViewOutline;

import java.io.IOException;

public class HelloApplication extends Application {
  Hand hand = new Hand();
  @Override
  public void start(Stage stage) throws IOException {
    Label title = new Label("Card Game");
    VBox buttons = new VBox(10);
    HBox cards = new HBox(10);
    VBox checks = new VBox(10);
    TextField inputField = new TextField();
    inputField.setPromptText("Enter number of cards");
    inputField.setMaxWidth(500);

    Label sum = new Label("");
    Label hearts = new Label("");
    Label QS = new Label("");
    Label checked = new Label("");



    Button button1 = new Button("Deal Hand");
    Button button2 = new Button("Check Hand");

    title.setFont(new Font("Arial", 50));
    sum.setFont(new Font("Arial", 30));
    hearts.setFont(new Font("Arial", 30));
    QS.setFont(new Font("Arial", 30));
    checked.setFont(new Font("Arial", 30));
    checked.setAlignment(Pos.CENTER);
    buttons.getChildren().addAll(inputField, button1, button2);


    button1.setOnAction(e -> {
      System.out.println("Dealing Hand");
      int numCards = Integer.parseInt(inputField.getText());
      cards.getChildren().clear();
      DeckOfCards deck = new DeckOfCards();
      hand = new Hand();
      for (PlayingCard card : deck.dealHand(numCards)) {
        CardViewOutline cardView = new CardViewOutline(String.valueOf(card.getFace()), String.valueOf(card.getSuit()));
        cards.getChildren().add(cardView.getCard());
        hand.addCard(card);
      }


    });
    button2.setOnAction(e -> {
      System.out.println("Checking Hand");
      if (hand != null) {
        checked.setText(hand.checkHand());
        sum.setText("Sum: " + hand.checkSum());
        hearts.setText("Hearts: " + hand.getHearts());
        if (hand.isSQ()) {
          QS.setText("Queen of Spades: True");
        } else {
          QS.setText("Queen of Spades: False");
        }
        checks.getChildren().addAll(checked, sum, hearts, QS);

      }
    });


    BorderPane root = new BorderPane();
    root.setTop(title);
    root.setLeft(buttons);
    root.setCenter(cards);
    root.setBottom(checks);

    Scene scene = new Scene(root, 1080, 720);
    stage.setTitle("Card Game");
    stage.setScene(scene);
    stage.show();
  }

  public static void main(String[] args) {
    launch();
  }
}
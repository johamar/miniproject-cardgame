package stud.ntnu.idatt2003.kortspill4.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class DeckOfCards {
  private final List<PlayingCard> deck;

  public DeckOfCards() {
    deck = new ArrayList<>();
    char[] suits = {'S', 'H', 'D', 'C'};

    for (char suit : suits) {
      for (int face = 1; face <= 13; face++) {
        deck.add(new PlayingCard(suit, face));
      }
    }
  }
  public List<PlayingCard> getDeck() {
    return deck;
  }
  public Collection<PlayingCard> dealHand(int numberOfCards) {
    if (numberOfCards < 0 || numberOfCards > deck.size()) {
      throw new IllegalArgumentException("Number of cards must be between 0 and " + deck.size() + " (inclusive)");
    }
    Collection<PlayingCard> hand = new ArrayList<>();
    Random random = new Random();
    for (int i = 0; i < numberOfCards; i++) {
      int cardIndex = random.nextInt(deck.size());
      hand.add(deck.remove(cardIndex));
    }
    return hand;
  }
}

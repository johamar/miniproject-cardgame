package stud.ntnu.idatt2003.kortspill4.model;

import java.util.*;

public class Hand {
  private List<PlayingCard> cards;

  public Hand() {
    this.cards = new ArrayList<>();
  }
  public boolean isFlush() {
    char suit = cards.get(0).getSuit();
    for (PlayingCard card : cards) {
      if (card.getSuit() != suit) {
        return false;
      }
    }
    return true;
  }

  public boolean isStraight() {
    List<Integer> faces = new ArrayList<>();
    for (PlayingCard card : cards) {
      faces.add(card.getFace());
    }
    Collections.sort(faces);
    for (int i = 0; i < faces.size() - 1; i++) {
      if (faces.get(i) + 1 != faces.get(i + 1)) {
        return false;
      }
    }
    return true;
  }

  public boolean isPair() {
    Set<Integer> uniqueFaces = new HashSet<>();
    for (PlayingCard card : cards) {
      uniqueFaces.add(card.getFace());
    }
    return uniqueFaces.size() < cards.size();
  }

  public boolean isTwoPair() {
    Map<Integer, Integer> faceCounts = new HashMap<>();
    for (PlayingCard card : cards) {
      faceCounts.put(card.getFace(), faceCounts.getOrDefault(card.getFace(), 0) + 1);
    }
    int pairCount = 0;
    for (int count : faceCounts.values()) {
      if (count == 2) {
        pairCount++;
      }
    }
    return pairCount == 2;
  }
  public boolean isThreeOfAKind() {
    Map<Integer, Integer> faceCounts = new HashMap<>();
    for (PlayingCard card : cards) {
      faceCounts.put(card.getFace(), faceCounts.getOrDefault(card.getFace(), 0) + 1);
    }
    for (int count : faceCounts.values()) {
      if (count == 3) {
        return true;
      }
    }
    return false;
  }
  public boolean isFourOfAKind() {
    Map<Integer, Integer> faceCounts = new HashMap<>();
    for (PlayingCard card : cards) {
      faceCounts.put(card.getFace(), faceCounts.getOrDefault(card.getFace(), 0) + 1);
    }
    for (int count : faceCounts.values()) {
      if (count == 4) {
        return true;
      }
    }
    return false;
  }
  public boolean isFullHouse() {
    Map<Integer, Integer> faceCounts = new HashMap<>();
    for (PlayingCard card : cards) {
      faceCounts.put(card.getFace(), faceCounts.getOrDefault(card.getFace(), 0) + 1);
    }
    boolean hasThreeOfAKind = false;
    boolean hasPair = false;
    for (int count : faceCounts.values()) {
      if (count == 3) {
        hasThreeOfAKind = true;
      }
      if (count == 2) {
        hasPair = true;
      }
    }
    return hasThreeOfAKind && hasPair;
  }
  public boolean isStraightFlush() {
    return isStraight() && isFlush();
  }
  public boolean isRoyalFlush() {
    if (!isStraightFlush()) {
      return false;
    }
    List<Integer> faces = new ArrayList<>();
    for (PlayingCard card : cards) {
      faces.add(card.getFace());
    }
    Collections.sort(faces);
    return faces.get(0) == 1 && faces.get(1) == 10;
  }
  public String checkHand() {
    if (isRoyalFlush()) {
      return "Royal Flush";
    }
    if (isStraightFlush()) {
      return "Straight Flush";
    }
    if (isFourOfAKind()) {
      return "Four of a Kind";
    }
    if (isFullHouse()) {
      return "Full House";
    }
    if (isFlush()) {
      return "Flush";
    }
    if (isStraight()) {
      return "Straight";
    }
    if (isThreeOfAKind()) {
      return "Three of a Kind";
    }
    if (isTwoPair()) {
      return "Two Pair";
    }
    if (isPair()) {
      return "Pair";
    }
    return "High Card";
  }
  public void addCard(PlayingCard card) {
    this.cards.add(card);
  }
  public int checkSum() {
    int sum = 0;
    for (PlayingCard card : cards) {
      sum += card.getFace();
    }
    return sum;
  }
  public String getHearts() {
    StringBuilder hearts = new StringBuilder();
    for (PlayingCard card : cards) {
      if (card.getSuit() == 'H') {
        hearts.append(card.getAsString()).append(" ");
      }
    }
    return hearts.toString();
  }
  public boolean isSQ() {
    for (PlayingCard card : cards) {
      if (card.getAsString().equals("S12")) {
        return true;
      }
    }
    return false;
  }

  public List<PlayingCard> getCards() {
    return cards;
  }
}

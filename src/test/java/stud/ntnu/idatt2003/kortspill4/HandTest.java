package stud.ntnu.idatt2003.kortspill4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import stud.ntnu.idatt2003.kortspill4.model.Hand;
import stud.ntnu.idatt2003.kortspill4.model.PlayingCard;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class HandTest {
  @Test
  public void testConstructorPositive() {
    Hand hand = new Hand();
    assertNotNull(hand, "Hand should not be null");
  }

  @Test
  public void testAddCardPositive() {
    Hand hand = new Hand();
    PlayingCard card = new PlayingCard('H', 1);
    hand.addCard(card);
    assertTrue(hand.getCards().contains(card), "Hand should contain the added card");
  }

  @Test
  public void testCheckSumPositive() {
    Hand hand = new Hand();
    PlayingCard card1 = new PlayingCard('H', 1);
    PlayingCard card2 = new PlayingCard('H', 2);
    hand.addCard(card1);
    hand.addCard(card2);
    assertEquals(3, hand.checkSum(), "Sum should be 3 after adding cards with face values 1 and 2");
  }

  @Test
  public void testIsSQPositive() {
    Hand hand = new Hand();
    PlayingCard card = new PlayingCard('S', 12);
    hand.addCard(card);
    assertTrue(hand.isSQ(), "isSQ should return true when the hand contains the Queen of Spades");
  }

  @Test
  public void testIsSQNegative() {
    Hand hand = new Hand();
    PlayingCard card = new PlayingCard('H', 1);
    hand.addCard(card);
    assertFalse(hand.isSQ(), "isSQ should return false when the hand does not contain the Queen of Spades");
  }
}

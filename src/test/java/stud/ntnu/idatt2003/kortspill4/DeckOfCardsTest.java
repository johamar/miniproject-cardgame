package stud.ntnu.idatt2003.kortspill4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import stud.ntnu.idatt2003.kortspill4.model.DeckOfCards;

import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardsTest {

  @Test
  public void testDeckSize() {
    DeckOfCards deck = new DeckOfCards();
    assertEquals(52, deck.getDeck().size());
  }
  @Test
  public void testDealHand() {
    DeckOfCards deck = new DeckOfCards();
    assertEquals(5, deck.dealHand(5).size());
  }
  @Test
  public void testGetDeckNotNull() {
    DeckOfCards deck = new DeckOfCards();
    assertNotNull(deck.getDeck(), "Deck should not be null");
  }

  @Test
  public void testDealHandMoreThanDeckSize() {
    DeckOfCards deck = new DeckOfCards();
    assertThrows(IllegalArgumentException.class, () -> deck.dealHand(53), "Should throw exception when dealing more cards than available in the deck");
  }

  @Test
  public void testDealHandNegativeNumber() {
    DeckOfCards deck = new DeckOfCards();
    assertThrows(IllegalArgumentException.class, () -> deck.dealHand(-1), "Should throw exception when dealing a negative number of cards");
  }
}


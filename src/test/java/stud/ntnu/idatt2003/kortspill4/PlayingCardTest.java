package stud.ntnu.idatt2003.kortspill4;

import org.junit.jupiter.api.Test;
import stud.ntnu.idatt2003.kortspill4.model.PlayingCard;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PlayingCardTest {
  @Test
  public void testConstructorPositive() {
    PlayingCard card = new PlayingCard('H', 1);
    assertEquals('H', card.getSuit(), "Suit should be Hearts");
    assertEquals(1, card.getFace(), "Face should be 1");
  }

  @Test
  public void testConstructorNegative() {
    assertThrows(IllegalArgumentException.class, () -> new PlayingCard('A', 1), "Should throw exception when suit is empty");
    assertThrows(IllegalArgumentException.class, () -> new PlayingCard('H', 0), "Should throw exception when face is empty");
  }

  @Test
  public void testGetSuitPositive() {
    PlayingCard card = new PlayingCard('H', 1);
    assertEquals('H', card.getSuit(), "Suit should be Hearts");
  }

  @Test
  public void testGetRankPositive() {
    PlayingCard card = new PlayingCard('H', 1);
    assertEquals(1, card.getFace(), "Face should be 1");
  }
}
